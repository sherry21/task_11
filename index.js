
var emails = ['gokhu@consultadd.com', 'yv@gmail.com', 'shreya1215@consultadd.com', 'shreyasoni@gmail.com', 'tanishq@consultadd.com','p@consultadd.com']

var newDiv = document.createElement("div");
newDiv.id = "mydiv";
document.body.appendChild(newDiv);

var head = document.createElement("h3");
head.appendChild(document.createTextNode("Checking Email"));
newDiv.appendChild(head);

newDiv.appendChild(document.createElement("hr"));

var content = document.createElement("p");
content.appendChild(document.createTextNode("Javascript Task"));
newDiv.appendChild(content);

var input = document.createElement("INPUT");
input.setAttribute("type", "email");
input.id = "email_id";
input.name = "email_id";
input.setAttribute('placeholder', 'Enter your email');
input.required = "required";

input.onkeyup = function() {
    var element = document.getElementById("email_id").value;
    ValidateEmail(element);
};

input.onmouseover = function() {
    input.style.color = '#FFFFFF';
    input.style.backgroundColor = '#E5E5E5';
};

input.onfocus = function() {
    input.style.color = '#000000';
    input.style.backgroundColor = '#FF3636';
};

input.onblur = function() {
    input.style.color = '#000000';
    input.style.backgroundColor = '#88FC81';
};

newDiv.appendChild(input);


var btn = document.createElement("BUTTON");
btn.setAttribute("type", "button"); //submit
btn.innerHTML = "Submit";
newDiv.appendChild(btn);
btn.onclick = function() { callAPI(); }

//Validate Email
function ValidateEmail(mail) {
    var flag = 0;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {    
        document.getElementsByTagName("p")[0].innerHTML = "Correct Format of email you can submit";
        for (var i = 0; i < emails.length; i++) {
            if (mail === emails[i]) {
                flag = 1;
                if (/^\w+([\.-]?\w+)*@consultadd.com$/.test(mail))
                    flag = 2;
            }
        }if (flag === 1) {
            document.getElementsByTagName("p")[0].innerHTML = "Email ID Exist as User";
        } else if (flag === 2) {
            document.getElementsByTagName("p")[0].innerHTML = "Email ID Exist as Consultadd (True)"   
        } else {
            document.getElementsByTagName("p")[0].innerHTML = "Email ID Does Not Exist as Consultadd (False)"
        }
    } else {
        document.getElementsByTagName("p")[0].innerHTML = "Wrong email Format";
    }
};

function callAPI() {
    var name,age;
    const fetchR = fetch("https://demo7857661.mockable.io/testdata");
    fetchR.then(response => {
        return response.json();
    }).then(testdata => {
        name = testdata.firstName;
        age = testdata.age;
        localStorage.setItem('textName', name);
        sessionStorage.setItem('age',age);
        window.location.assign("home.html?age="+age); 
    })
};